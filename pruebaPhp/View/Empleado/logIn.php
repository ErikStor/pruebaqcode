<link rel="stylesheet" type="text/css" media="screen" href="Web/css/core.css">

<div class="container">

<div class="col-12">
    <form class="cmxform" id="commentForm" method="post" action="<?php echo getUrl("Empleado","Empleado","logInProcess");?>">
        <br>
		<fieldset>
            <legend>Iniciar sesi&oacute;n</legend>

            <?php
                if(isset($_SESSION['error']['message'])){            
            ?>
            
            <div class="" >
                <div class="alert alert-danger" role="alert">
                    <?php echo $_SESSION['error']['message']; ?>
                </div>
            </div>
            <?php
                unset($_SESSION['error']);
                }   
            ?>


            <?php
                if(isset($_SESSION['success'])){            
            ?>
            
            <div class="" >
                <div class="alert alert-success" role="alert">
                    <?php echo $_SESSION['success']; ?>
                </div>
            </div>
            <?php
                    unset($_SESSION['success']);
                }   
            ?>


            <div class="form-group">
                <label for="email">Email *</label>
				<input class="form-control" id="email" name="email" minlength="2" maxlength="255" type="email" required placeholder="Email">
            </div>
            <div class="form-group">	
                <label for="password">Contraseña *</label>
                <input class="form-control" id="password" type="password" minlength="2" maxlength="255" name="password" required>
            </div>
            <div class="form-group">
                <button class="form-control btn btn-success" type="submit">Iniciar sesi&oacute;n</button>
            </div>
		</fieldset>
	</form>
</div>



</div>