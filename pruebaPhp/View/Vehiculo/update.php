<div class="container">

<div class="col-12">
    <form class="cmxform" id="commentForm"  enctype="multipart/form-data"method="post" action="<?php echo getUrl("Vehiculo","Vehiculo","postUpdate");?>">

    
    <?php if($veh = mysqli_fetch_assoc($vehiculo)){?>

        <br>
		<fieldset>
            <legend>Actualizar veh&iacute;culo</legend>

            <?php
                if(isset($_SESSION['error']['message'])){            
            ?>
            
            <div class="" >
                <div class="alert alert-danger" role="alert">
                    <?php echo $_SESSION['error']['message']; ?>
                </div>
            </div>
            <?php
                unset($_SESSION['error']);
                }   
            ?>


            <?php
                if(isset($_SESSION['success'])){            
            ?>
            
            <div class="" >
                <div class="alert alert-success" role="alert">
                    <?php echo $_SESSION['success']; ?>
                </div>
            </div>
            <?php
                    unset($_SESSION['success']);
                }   
            ?>


            <div class="form-group">
                <label for="nombre">Nombre *</label>
				<input class="form-control" id="nombre" name="nombre" value="<?php echo $veh['nombre'] ?>" minlength="2" maxlength="255"type="text" required placeholder="Nombre">
            </div>
            <div class="form-group">
                <label for="precio">Precio *</label>
				<input class="form-control" id="precio" name="precio" min="1" type="number" required placeholder="precio" value="<?php echo $veh['precio'] ?>">
            </div>
            <div class="form-group">
                <label for="modelo">Modelo *</label>
				<input class="form-control" id="modelo" name="modelo" min="1" max="2020" type="number" required placeholder="Modelo" value="<?php echo $veh['modelo'] ?>">
            </div>
            <div class="form-group">	
                <label for="referencia">Imagen *</label>
                <div class="col-12 form-group">
                    <img class='sizeImg' src="<?php echo $veh['img'] ?>" alt="Imagen del veh&iacute;culo <?php echo $veh['nombre'] ?>">
                </div>                
                <div class="col-12">
                    <input class="form-control" id="img" type="file" minlength="2" maxlength="255" name="img">
                </div>
            </div>
            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $veh['id']?>">
                <button class="form-control btn btn-success" type="submit">Actualizar</button>
            </div>
		</fieldset>
        <?php 
    
        }

        ?>
	</form>
    </div>
</div>



   

</div>
