
<div class="container">

<div class="col-12">
    <form class="cmxform" id="commentForm"  enctype="multipart/form-data" method="post" action="<?php echo getUrl("Vehiculo","Vehiculo","getFormCargaMasivaProcess");?>">
        <br>
		<fieldset>
            <legend>Carga masiva de veh&iacute;culos</legend>

            <?php
                if(isset($_SESSION['error']['message'])){            
            ?>
            
            <div class="" >
                <div class="alert alert-danger" role="alert">
                    <?php echo $_SESSION['error']['message']; ?>
                </div>
            </div>
            <?php
                unset($_SESSION['error']);
                }   
            ?>


            <?php
                if(isset($_SESSION['success'])){            
            ?>
            
            <div class="" >
                <div class="alert alert-success" role="alert">
                    <?php echo $_SESSION['success']; ?>
                </div>
            </div>
            <?php
                    unset($_SESSION['success']);
                }   
            ?>

            <div class="form-group">	
                <label for="file">Archivo *</label>
                <input class="form-control" id="file" name='file' type="file" required>
            </div>
            <div class="form-group">
                <button class="form-control btn btn-success" type="submit">Enviar</button>
            </div>
		</fieldset>
	</form>
</div>



</div>
