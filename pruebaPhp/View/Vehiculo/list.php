<br>
<br>
<div class="container">

    <?php
        if(isset($_SESSION['error']['message'])){            
    ?>
            
            <div class="" >
                <div class="alert alert-danger" role="alert">
                    <?php echo $_SESSION['error']['message']; ?>
                </div>
            </div>
    <?php
        unset($_SESSION['error']);
        }   
    ?>


    <?php
        if(isset($_SESSION['success'])){            
    ?>
    <div class="col-12" >
        <div class="alert alert-success" role="alert">
            <?php echo $_SESSION['success']; ?>
        </div>
    </div>
    <?php
            unset($_SESSION['success']);
        }   
    ?>

    <div>
        <table class="table table-striped">
            <thead class="">
                    <tr>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Imagen</th>
                        <th>Opciones</th>
                    </tr>  
            </thead>  
            <tbody>
                <?php
                    while($veh = mysqli_fetch_assoc($vehiculo)){
                        echo "<tr>";
                        echo "<td>".$veh['nombre']."</td>";
                        echo "<td>".$veh['precio']."</td>";
                        echo "<td><img src='".$veh['img']."' class='sizeImg'></td>";
                        echo "<td>
                                <a  href=". getUrl("Vehiculo","Vehiculo","getUpdate", array('id'=>$veh['id'])) . " >Actualizar Vehiculo</a>
                                <a  href=". getUrl("Vehiculo","Vehiculo","getDelete", array('id'=>$veh['id'])) . " >Eliminar Vehiculo</a>
                              </td>";
                        echo "</tr>";
                    }                
                ?>
            </tbody>
        </table>
    </div>
</div>