<div class="container">

<div class="col-12">
    <form class="cmxform" id="commentForm"  enctype="multipart/form-data" method="post" action="<?php echo getUrl("Vehiculo","Vehiculo","postCreate");?>">
        <br>
		<fieldset>
            <legend>Registro de veh&iacute;culo</legend>

            <?php
                if(isset($_SESSION['error']['message'])){            
            ?>
            
            <div class="" >
                <div class="alert alert-danger" role="alert">
                    <?php echo $_SESSION['error']['message']; ?>
                </div>
            </div>
            <?php
                unset($_SESSION['error']);
                }   
            ?>


            <?php
                if(isset($_SESSION['success'])){            
            ?>
            
            <div class="" >
                <div class="alert alert-success" role="alert">
                    <?php echo $_SESSION['success']; ?>
                </div>
            </div>
            <?php
                    unset($_SESSION['success']);
                }   
            ?>


            <div class="form-group">
                <label for="nombre">Nombre *</label>
				<input class="form-control" id="nombre" name="nombre" minlength="2" maxlength="255"type="text" required placeholder="Nombre">
            </div>
            <div class="form-group">
                <label for="nombre">Modelo *</label>
				<input class="form-control" id="modelo" name="modelo" min="1" max="2020" type="number" required placeholder="Modelo">
            </div>
            <div class="form-group">	
                <label for="img">Imagen *</label>
                <input class="form-control" id="img" type="file" minlength="2" maxlength="255" name="img" required>
            </div>
            <div class="form-group">
                <button class="form-control btn btn-success" type="submit">Enviar</button>
            </div>
		</fieldset>
	</form>
</div>



</div>
