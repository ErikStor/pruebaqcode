<div class="container">

<div class="col-12">
    <form class="cmxform" id="commentForm" method="post" action="<?php echo getUrl("Vehiculo","Vehiculo","postDelete");?>">

    
    <?php if($veh = mysqli_fetch_assoc($vehiculo)){?>

        <br>
		<fieldset>
            <legend>Eliminar veh&iacute;culo</legend>

            <?php
                if(isset($_SESSION['error']['message'])){            
            ?>
            
            <div class="" >
                <div class="alert alert-danger" role="alert">
                    <?php echo $_SESSION['error']['message']; ?>
                </div>
            </div>
            <?php
                unset($_SESSION['error']);
                }   
            ?>


            <?php
                if(isset($_SESSION['success'])){            
            ?>
            
            <div class="" >
                <div class="alert alert-success" role="alert">
                    <?php echo $_SESSION['success']; ?>
                </div>
            </div>
            <?php
                    unset($_SESSION['success']);
                }   
            ?>


            <div class="form-group">
                <label for="nombre">Nombre *</label>
				<input class="form-control"  disabled id="nombre" name="nombre" value="<?php echo $veh['nombre'] ?>" minlength="2" maxlength="255"type="text" required placeholder="Nombre">
            </div>
            <div class="form-group">
                <label for="nombre">Precio *</label>
				<input class="form-control" disabled id="modelo" name="modelo" min="1" max="2020" type="number" required placeholder="Modelo" value="<?php echo $veh['precio'] ?>">
            </div>
            <div class="form-group">
                <label for="nombre">Modelo *</label>
				<input class="form-control" disabled id="modelo" name="modelo" min="1" max="2020" type="number" required placeholder="Modelo" value="<?php echo $veh['modelo'] ?>">
            </div>
            <div class="form-group">	
                <label for="referencia">Imagen *</label>
                <div class="col-12">
                    <img class="sizeImg" src="<?php echo $veh['img'] ?>" alt="Imagen del veh&iacute;culo <?php echo $veh['nombre'] ?>">
                </div>                
            </div>
            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $veh['id']?>">
                <button class="form-control btn btn-danger" type="submit">Eliminar</button>
            </div>
		</fieldset>
        <?php 
    
        }

        ?>
	</form>
    </div>
</div>



   

</div>
