<?php

include_once 'Model/EmpleadoModel/EmpleadoModel.php';

class EmpleadoController{

        public function logIn()
        {
            include_once "View/Empleado/logIn.php";
        }

        public function logOut(){
            if(isset($_SESSION['empleado'])){
                session_destroy();
                redirect("index.php");
            }    
        }

        public function logInProcess(){
            
            if(isset($_POST['email']) && isset($_POST['password'])){
               
                extract($_POST);
                    
                $sql="select * from empleado where email='$email' and password='$password'";

                $empleado =new EmpleadoModel();
                
                $inicio = $empleado->consultar($sql);
                
                if($log = mysqli_fetch_assoc($inicio)){
                   $_SESSION['empleado'] = true;    
                }
                
                if(!isset($_SESSION['empleado']) || $_SESSION['empleado'] !== true){
                    $_SESSION['error']['message'] = "El usuario y/o contraseña no son validos";
                    redirect(getUrl("Empleado","Empleado","logIn"));
                }

                redirect(getUrl("Vehiculo","Vehiculo","getList"));

            }else{

                $_SESSION['error']['message'] = "El usuario y contraseña son obligatorios";

                redirect(getUrl("Empleado","Empleado","logIn"));
            }
        }



}

?>
