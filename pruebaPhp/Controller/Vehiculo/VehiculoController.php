<?php

include_once 'Model/VehiculoModel/VehiculoModel.php';

class VehiculoController{

        public function getList(){
            $obj = new VehiculoModel();

            $sql="select * from vehiculo";
            $vehiculo = $obj ->consultar($sql);
            $obj->close();

            include_once "View/Vehiculo/list.php";
        }

        public function getFormCreate(){
          include_once "View/Vehiculo/create.php";
        }

        public function postCreate(){

            $obj = new VehiculoModel();

            $_SESSION['error'] = $_POST;

            $url = getUrl("Vehiculo","Vehiculo","getFormCreate");

            //Validaciones
            if(!preg_match("/([a-zA-Z])\w+/",$_POST['nombre'])){
                $_SESSION['error']['message'] = "El nombre debe de ser un texto sin signos carácteres especiales";
                redirect($url);
            }

            //Insercion
            extract($_POST);

            $archivo = $_FILES['img']['tmp_name'];

            $ruta = "Web/img/".time().$_FILES['img']['name'];
            move_uploaded_file($archivo,$ruta);
            
            $precio = 200000;

            if(date('j') % 2 === 0){
                $precio = $precio + ($precio * 0.05);
            }
            
            if($modelo <= 1997){
                $precio = $precio + ($precio * 0.20);
            }
            
            $sql = "insert into Vehiculo (nombre, precio, modelo, img)
             values ('".$nombre."', ".$precio.",".$modelo.", '".$ruta."')";

            $obj->insertar($sql);
            $obj->close($sql);

            //Eliminacion de la variable de sesion de errores
            unset($_SESSION['error']);

            $_SESSION['success'] = 'Se registro el vehiculo correctamente';
            redirect($url);
        }

        public function getUpdate(){
            if(isset($_GET['id']) && $_GET['id']!=""){
                $obj = new VehiculoModel();

                $sql="select *
                      from Vehiculo as e
                      where e.id=".$_GET['id'];

                $vehiculo = $obj->consultar($sql);
                $obj->close();

                include_once 'View/Vehiculo/update.php';
            }
        }

        public function postUpdate(){
            if(isset($_POST)){
                
                $obj = new VehiculoModel();

                $_SESSION['error'] = $_POST;
    
                $url = getUrl("Vehiculo","Vehiculo","getUpdate",array('id'=>$_POST['id']));
    
                //Validaciones
                if(!preg_match("/([a-zA-Z])\w+/",$_POST['nombre'])){
                    $_SESSION['error']['message'] = "El nombre debe de ser un texto sin signos carácteres especiales";
                    redirect($url);
                }
    
                //actualizacion
                extract($_POST);

                $sql="select *
                from Vehiculo as e
                where e.id=".$id;

                $vehiculo = $obj->consultar($sql);

                if($veh = mysqli_fetch_assoc($vehiculo)){
                    $ruta = $veh['img'];
                }
                
                if(isset($_FILES['size']) && $_FILES['size'] > 0){
                    
                    $archivo = $_FILES['img']['tmp_name'];
    
                    $ruta = "Web/img/".time().$_FILES['img']['name'];
                    move_uploaded_file($archivo,$ruta);    
                }
              
                $sql = "UPDATE vehiculo
                SET nombre = '".$nombre."',
                    precio = '".$precio."',
                    modelo = '".$modelo."',
                    img = '".$ruta."'
                WHERE id='$id';";
    
                $obj->editar($sql);

                $obj->close();
                //Eliminacion de la variable de sesion de errores
                unset($_SESSION['error']);

                $_SESSION['success'] = 'Se actualiz&oacute; el Vehiculo correctamente';
                redirect($url);
            }
        }

        public function getDelete(){
            if(isset($_GET['id']) && $_GET['id']!=""){
                $obj = new VehiculoModel();

                $sql="select *
                      from vehiculo as e
                      where e.id=".$_GET['id'];

                $vehiculo = $obj->consultar($sql);
                $obj->close();

                include_once 'View/Vehiculo/delete.php';
            }
        }

        public function postDelete(){
            if(isset($_POST)){

                $obj = new VehiculoModel();

                $_SESSION['error'] = $_POST;

                extract($_POST);

                $url = getUrl("Vehiculo","Vehiculo","getDelete", array("id"=>$id));

                //Validacion
                if(!is_numeric($_POST['id'])){
                    $_SESSION['error']['message'] = "El vehiculo debe tener un identificador";
                    redirect($url);
                }

                //Eliminado
                $sql = "delete from vehiculo where vehiculo.id=$id";

                $obj->eliminar($sql);
                $obj->close();   
                //Eliminacion de la variable de sesion de errores
                unset($_SESSION['error']);

                $_SESSION['success'] = 'Se elimin&oacute; el vehiculo correctamente';
                redirect(getUrl("Vehiculo","Vehiculo","getList"));
            }
        }

        public function getFormCargaMasiva(){
            include_once "View/Vehiculo/cargaMasiva.php";
        }

        public function getFormCargaMasivaProcess(){

            $fileContent = file_get_contents($_FILES['file']['tmp_name']);

            $formatContent = str_getcsv($fileContent,"\n");
            
            $control = 0;
            $obj = new VehiculoModel();

            foreach($formatContent as $item){

                if($control > 0){
                    
                    $vehiculo = str_getcsv($item,",");

                    $sql="select id
                    from Vehiculo as v
                    where v.nombre='".$vehiculo[0]."'
                    AND v.modelo=".$vehiculo[1];
                    
                    $search = $obj->consultar($sql);

                    $id = null;

                    if($veh = mysqli_fetch_assoc($search)){
                        $id = $veh['id'];
                    }

                    if($id !== null){

                        $sql = "UPDATE vehiculo
                        SET nombre = '".$vehiculo[0]."',
                            precio = '".$vehiculo[2]."',
                            modelo = '".$vehiculo[1]."'
                        WHERE id='$id';";
            
                        $obj->editar($sql);
                      

                    }else{
                        $sql = "insert into vehiculo (nombre, precio, modelo)
                        values ('".$vehiculo[0]."', ".$vehiculo[2].",".$vehiculo[1].")";
           
                        $obj->insertar($sql);
                    }

                }else {
                    $control++;
                }

                
            }           
           
            $obj->close();   
            
            $_SESSION['success'] = 'Se registraron y/o actualizaron los veh&iacute;culos correctamente';
            redirect(getUrl("Vehiculo","Vehiculo","getList"));
        }
        
}

?>