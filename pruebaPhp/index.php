<?php
    include_once 'Lib/helper.php';
    include_once 'View/Partials/header.php';
    session_start();
    
    if(isset($_GET['modulo'])){

        if(isset($_SESSION['empleado'])){
            include_once 'View/Partials/router.php';
        }
        
        resolve();
    }else{
        include_once 'View/Empleado/logIn.php';
    }

    include_once 'View/Partials/footer.php';

?>
