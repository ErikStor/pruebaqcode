import React from 'react';
import './App.css';

import Valor from './components/Valor';
import Select from './components/Select';

var handleClick = event => {
  event.preventDefault();

  let valor = document.getElementById('Valor');
  let trm = document.getElementById('TRM');
  let select = document.getElementById('seleccione');
  
  let format = document.getElementsByClassName('format');

  for(let i = 0; i < format.length; i++){
    format[i].innerHTML = 0;
  }

  valor.value = 0;
  trm.value = 0;
  select.setAttribute('selected',"");
  
}

function App() {
  return (
    <div>
      <form >
        <Valor id="Valor" label="Valor" />
        
        <Valor id="TRM" label="TRM" />

        <Select id="select" />

        <button onClick={handleClick}>Guardar</button>
      </form>      
    </div>
  );
}

export default App;
