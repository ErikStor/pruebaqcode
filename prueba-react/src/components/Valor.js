import React, {Component} from 'react'

function FormatNumber({ number }) {
    return (
      <span className="format">
        {new Intl.NumberFormat("en-IN").format(number)}
      </span>
    );
}

class Valor extends Component {
    
  constructor(props) {
    super(props);

    this.label = {value:props.label}
    this.id = {value:props.id}
  }

    state = {
        number: 0
    };
    
      handleChange = event => {
        this.setState({
          number: event.target.value > 999999999 ? 999999999 : event.target.value,
        });
      };

      
      
      render() {
        const { number } = this.state;
        
        return (
          <div className="App">
            <h1>
                <FormatNumber number={number} />
            </h1>
            <label>{this.label.value}
              <input id={this.id.value} type="number" onChange={this.handleChange}  />
            </label>
          </div>
        );
      }
  }

export default Valor;


