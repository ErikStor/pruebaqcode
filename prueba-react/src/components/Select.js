import React, {Component} from 'react'

class Select extends Component {


    constructor(props) {
        super(props);
        this.id = {value:props.id}
    }
    

    state = {
        fruit: "#"
    };

    handleChange = event => {
        this.setState({
            fruit: event.target.value !== "#" ? event.target.value : "#"
        });
    }


    render() {
        return (
          <div className="App">
              <label>
                <select id={this.id.value} onChange={this.handleChange.bind(this)}>
                    <option id="seleccione" value="#">Seleccione...</option>
                    <option value="grapefruit">Toronja</option>
                    <option value="lime">Lima</option>
                    <option value="coconut">Coco</option>
                    <option value="mango">Mango</option>
                </select>
            </label>            
          </div>
        );
    }
}

export default Select;


